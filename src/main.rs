use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderEvent, UpdateEvent};
//use piston::inoput::{RenderArgs, UpdateArgs};
use piston::window::WindowSettings;

use specs::prelude::*;
use specs_derive::*;

pub struct ClearSystem;

#[derive(SystemData)]
pub struct RenderData<'a> {
    gl: WriteExpect<'a, GlGraphics>,
    view: WriteExpect<'a, graphics::Viewport>,
}

impl<'a> System<'a> for ClearSystem {
    type SystemData = RenderData<'a>;
    fn run(&mut self, mut render: Self::SystemData) {
        use graphics::*;
        const BACKGROUND: [f32; 4] = [237.0 / 255.0,201.0 / 255.0,175.0 / 255.0, 1.0];
        render.gl.draw(*render.view, |_, gl| {
            clear(BACKGROUND, gl);
        });
    }
}

pub struct RenderBoidSystem;
impl<'a> System<'a> for RenderBoidSystem {
    type SystemData = (
        RenderData<'a>,
        ReadStorage<'a, Boid>,
        ReadStorage<'a, Position>,
    );
    fn run(&mut self, (mut render, boid, pos): Self::SystemData) {
        use graphics::*;
        const COL: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
        let square = rectangle::square(0.0, 0.0, 10.0);
        for (_boid, pos) in (&boid, &pos).join() {
            render.gl.draw(*render.view, |c, gl| {
                let transform = c.transform.trans(pos.0.x, pos.0.y);
                rectangle(COL, square, transform, gl);
            });
        }
    }
}

pub struct MoveBoidSystem;

impl<'a> System<'a> for MoveBoidSystem {
    type SystemData = (
        ReadExpect<'a, piston::UpdateArgs>,
        ReadStorage<'a, Boid>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Velocity>,
    );
    fn run(&mut self, (args, boid, mut pos, vel): Self::SystemData) {
        for (_boid, pos, vel) in (&boid, &mut pos, &vel).join() {
            pos.0 += vel.0.scale(args.dt);
        }
    }
}

pub struct FlockSystem;

struct Size;

impl<'a> System<'a> for FlockSystem {
    type SystemData = (
        ReadExpect<'a, piston::UpdateArgs>,
        Entities<'a>,
        ReadStorage<'a, Boid>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Acceleration>,
    );
    fn run(&mut self, (args, ents, boid, pos, mut vel, mut acc): Self::SystemData) {
        const LOCAL_RANGE: f64 = 70.0;
        const MIN_RANGE: f64 =  0.0000001;
        const SEP_FACTOR: f64 = 30.0;
        const FLOCK_FACTOR: f64 = 5.0;
        const ALIGN_FACTOR: f64 = 1.0;
        const MAX_SPEED: f64 = 80.0;
        const MAX_FORCE: f64 = 100.0;
        const FORCE_FACTOR: f64 = 10.0;

        use aabb_quadtree::{QuadTree, ItemId};
        use euclid::{TypedRect, TypedSize2D, TypedPoint2D};
        let size: TypedRect<f32, Size> = TypedRect::from_size(TypedSize2D::new(1000.0, 1000.0));
        type BackingArray = [(ItemId, TypedRect<f32, Size>); 512];
        let mut map: QuadTree<Entity, Size, BackingArray> = QuadTree::default(size, 2);
       
        for (_boid, Position(p), ent) in (&boid, &pos, &ents).join() {
            let origin = TypedPoint2D::new(p.x as f32, p.y as f32);
            let boidsize = TypedSize2D::new(10.0, 10.0);
            map.insert_with_box(ent, TypedRect::new(origin, boidsize));
        }

        for (_boida, pos_self, acc) in (&boid, &pos, &mut acc).join() {
            let mut avg_vel = Velocity::default();
            let mut avg_rel_pos = Position::default();
            let mut sep_dir = Vector2::default();
            let mut locals: u32 = 0;
            for (_boidb, pos_other, vel_other) in (&boid, &pos, &vel).join() {
                let dist = pos_other.0 - pos_self.0;
                if dist.length() <= LOCAL_RANGE {
                    locals += 1;
                    avg_rel_pos.0 += dist;
                    avg_vel.0 += vel_other.0;
                    if dist.length() > MIN_RANGE {
                        sep_dir += (-dist).scale(SEP_FACTOR / dist.length());
                    }
                }
            }
            
            avg_rel_pos.0.scale_assign(1.0 / locals as f64);
            avg_vel.0.scale_assign(1.0 / locals as f64);
            
            acc.0 = Vector2::default();
            acc.0 += avg_vel.0.scale(ALIGN_FACTOR);
            acc.0 += avg_rel_pos.0.scale(FLOCK_FACTOR);
            acc.0 += sep_dir;

        }

        for (_boid, mut vel, acc) in (&boid, &mut vel, &mut acc).join() {
            vel.0 += acc.0.clamp(MAX_FORCE).scale(FORCE_FACTOR * args.dt);
            vel.0 = vel.0.clamp(MAX_SPEED);
        }
    }
}

pub struct ClampSystem;

impl<'a> System<'a> for ClampSystem {
    type SystemData = (
        ReadExpect<'a, WorldSize>,
        ReadStorage<'a, Boid>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
    );

    fn run(&mut self, (size, boid, pos, mut vel) : Self::SystemData) {
        const EDGE_SIZE: f64 = 20.0;
        let center = Vector2 {
            x: size.width as f64 / 2.0,
            y: size.height as f64 / 2.0,
        };
        for (_boid, pos, vel) in (&boid, &pos, &mut vel).join() {
            let to_center = center - pos.0;
            let dist_v = (pos.0 - center).abs() - center;
            let dist = if dist_v.x > dist_v.y { dist_v.x } else { dist_v.y } + EDGE_SIZE;
            if dist > 0.0 {
                vel.0 += to_center.normalize().scale(dist / EDGE_SIZE); 
            }
        }
    }
}

#[derive(Component, Debug, Default)]
#[storage(NullStorage)]
pub struct Boid;

#[derive(Debug, Default, Copy, Clone)]
pub struct Vector2 {
    pub x: f64,
    pub y: f64,
}

impl Vector2 {
    fn scale_assign(&mut self, scalar: f64) {
        *self = self.scale(scalar)
    }
    
    fn scale(self, scalar: f64) -> Self {
        Self {
            x: self.x * scalar,
            y: self.y * scalar,
        }
    }

    fn length(self) -> f64 {
        (self.x * self.x + self.y * self.y).sqrt() 
    }

    fn normalize(self) -> Self {
        self.scale(1.0 / self.length())
    }

    fn clamp(self, len: f64) -> Self {
        if self.length() > len {
            self.normalize().scale(len)
        } else {
            self
        }
    }

    fn abs(self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
        }
    }
}

impl std::ops::Neg for Vector2 {
    type Output = Vector2;
    fn neg(self) -> Self::Output {
        Self::Output {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl std::ops::AddAssign for Vector2 {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Add for Vector2 {
    type Output = Vector2;
    fn add(self, other: Vector2) -> Vector2 {
        Vector2 { x: self.x + other.x, y: self.y + other.y }
    }
}

impl std::ops::Sub for Vector2 {
    type Output = Vector2;
    fn sub(self, other: Vector2) -> Vector2 {
        Vector2 { x: self.x - other.x, y: self.y - other.y }
    }
}

#[derive(Component, Debug, Default, Clone)]
#[storage(VecStorage)]
pub struct Position(pub Vector2);

#[derive(Component, Debug, Default, Clone)]
#[storage(VecStorage)]
pub struct Velocity(pub Vector2);

#[derive(Component, Debug, Default, Clone)]
#[storage(VecStorage)]
pub struct Acceleration(pub Vector2);

#[derive(Debug, Default, Copy, Clone)]
pub struct WorldSize {
    width: u32,
    height: u32,
}

fn create_random_boid(world: &mut World, rng: &mut impl rand::Rng, range: &WorldSize) {
    world
        .create_entity()
        .with(Boid)
        .with(Position(Vector2 {
            x: rng.gen_range(0.0, range.width as f64),
            y: rng.gen_range(0.0, range.height as f64),
        })).with(Velocity(Vector2 {
            x: rng.gen_range(-1.0, 1.0),
            y: rng.gen_range(-1.0, 1.0),
        }))
        .with(Acceleration::default())
        .build();
}

fn populate_boids(world: &mut World) {
    let mut rng = rand::thread_rng();
    let size: WorldSize = *world.read_resource();
    let area = size.width * size.height;
    for _i in 0..area / 8000 {
        create_random_boid(world, &mut rng, &size);
    }
}

fn main() {
    println!("Hello, world!");
    let opengl = OpenGL::V3_2;
    const START_SIZE: WorldSize = WorldSize {
        width: 1000,
        height: 1000,
    };
    let mut window: Window =
        WindowSettings::new("spinning-square", [START_SIZE.width, START_SIZE.height])
            .graphics_api(opengl)
            .exit_on_esc(true)
            .build()
            .unwrap();

    let mut step = DispatcherBuilder::new()
        .with(FlockSystem, "FlockBoids", &[])
        .with(ClampSystem, "ClampAfter", &["FlockBoids"])
        .with(MoveBoidSystem, "MoveBoids", &["FlockBoids", "ClampAfter"])
        .build();

    let mut render = DispatcherBuilder::new()
        .with_thread_local(ClearSystem)
        .with_thread_local(RenderBoidSystem)
        .build();

    let mut world = World::new();
    world.insert(GlGraphics::new(opengl));
    world.insert(START_SIZE);

    step.setup(&mut world);
    render.setup(&mut world);

    populate_boids(&mut world);
    

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {

        if let Some(args) = e.render_args() {
            let view: graphics::Viewport = args.viewport();
            world.insert(view);
            render.dispatch(&world);
        }
        if let Some(args) = e.update_args() {
            world.insert(args);
            step.dispatch(&world);
            world.maintain();
        }
    }
}
