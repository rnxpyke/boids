# Boids

This is a simple flocking simulation, also known as [boids](https://en.wikipedia.org/wiki/Boids), written in rust.
In short, every boid, represented by a black dot in my programm, tries to to align its position and speed with it's neighbours acording to the rules of:
- **seperation**
- **alignment**
- **cohesion**

Additionaly to these normal rules, my implementation also has some rules to cage the boids to the screen region.


Currently the implementation is not very efficient, see the [issues](https://gitlab.com/rnxpyke/boids/-/issues) for details.

## Building

This is a rust (only) project, so the build steps are fairly simple. Install the `cargo` tool through your package manager or use the [official installation instructions](https://www.rust-lang.org/tools/install), then just run the following:

```bash
# build the project
cargo build

# run the project
cargo run
```


## References

See the `Cargo.toml` file for used libraries. The most work is done by [`specs`](https://slide-rs.github.io/specs-website/) an entity-component-system, and [`piston`](https://www.piston.rs/).
